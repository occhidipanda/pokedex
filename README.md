# README #

Pokedex, a simple view detail app showing all pokemon availabe through pokeApi!   

The app works both online and offline, it uses a "double" cache system for storing data: all api call responses are stored inside the filesystem (cache folder) and every time a resource is needed it will be loaded inside a NSCache object. In order to keep memory free, cache stored in the RAM will be cleared if a memory warning is raised.

images are stored only in the filesystem and not in NSCache obj.  

In order to execute a large amount of calls I created the network manager: a simple class that handle multiple api calls using OperationQueue, with this the app is able to do 3 (or more) calls simultaneously and wait till the last is done.
For networking i used a class made by me in the past, it was edited a bit for working as Operation class.

App uses paging for showing the complete list of pokemon, the item limit is 20 for iPhone and 50 for iPad 

UI inspired by https://dribbble.com/shots/6540871-Pokedex-App. 

I wanted to use zero external library but I found 'Anchorage' very useful for building layouts constraints.
I used cocoapods, the main reason is because im very familiar with it.

external library: 
    • Anchorage - I choose anchorage because it's an usefull tool full of utilities and operators that simplify Auto layout code. 
    
Unfortunately i noticed pokeApi doesent expose api for filtering data.

Enjoy!
Tiziano Caccavo. 
