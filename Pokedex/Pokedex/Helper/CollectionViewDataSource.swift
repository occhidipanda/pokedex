//
//  CollectionViewDataSource.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import UIKit

protocol ItemViewModel {
    var reuseIdentifier: String { get }
    func setup(_ cell: UICollectionViewCell, in collectionView: UICollectionView, at indexPath: IndexPath)
}

protocol PagingDelegate {
    var isUpdateRequired: Bool {get set}
    var isUpdating: Bool {get set}
    func shouldUpdateData()
}

class CollectionViewDataSource: NSObject, UICollectionViewDataSource {
    
    var delegate: PagingDelegate?
    var items: [ItemViewModel] = []
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model = self.items[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: model.reuseIdentifier, for: indexPath)
        model.setup(cell, in: collectionView, at: indexPath)
        
        if (delegate?.isUpdateRequired ?? false) && (delegate?.isUpdating ?? true) == false && indexPath.row == items.count - 1 {
            delegate?.shouldUpdateData()
        }
    
        return cell
    }

}
