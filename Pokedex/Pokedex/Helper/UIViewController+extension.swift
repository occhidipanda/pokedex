//
//  UIViewController+extension.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Anchorage

extension UIViewController {
    
    func add(to container: UIView, parent: UIViewController, padding: UIEdgeInsets = .zero){
        willMove(toParent: parent)
        parent.addChild(self)
        container.addSubview(view)
        didMove(toParent: parent)
        view.leftAnchor /==/ container.leftAnchor + padding.left
        view.rightAnchor /==/ container.rightAnchor - padding.right
        view.topAnchor /==/ container.topAnchor + padding.top
        view.bottomAnchor /==/ container.bottomAnchor - padding.bottom
    }
    
    func remove(){
        guard parent != nil else {return}
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}
