//
//  CacheManager.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import UIKit

class CacheManager: NSObject {
    
    static let shared = CacheManager()
    
    private var cacheURL: URL? {
        return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
    }
    private lazy var cache: NSCache<NSString, NSData> = {
        let cache = NSCache<NSString, NSData>()
        cache.countLimit = 300
        return cache
    }()
    
    private override init() {super.init()}
    
    func getCachedData(url: URL) throws -> Data?{
        if let data = cache.object(forKey: fileNameFrom(url: url) as NSString) {
            return data as Data
        }
        return try loadData(url: url,shouldCacheinMemory: true)
    }
    
    func getCachedImageData(url: URL) throws -> Data?{
        return try loadData(url: url, shouldCacheinMemory: false)
    }
    
    func cacheJSONData(_ data: Data, with url: URL) throws {
        cache.setObject(data as NSData, forKey: fileNameFrom(url: url) as NSString)
        try storeData(data: data, to: url)
    }
    
    func cacheImage(imageData data: Data, with url: URL) throws {
        try storeData(data: data, to: url)
    }
    
    func clearCurrentCache(){
        cache.removeAllObjects()
    }
    
    func deleteCache(){
        guard let cacheURL =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else { return }
        let fileManager = FileManager.default
        do {
            let directoryContents = try fileManager.contentsOfDirectory(at: cacheURL, includingPropertiesForKeys: nil, options: [])
            for file in directoryContents {
                do {
                    try fileManager.removeItem(at: file)
                } catch let error as NSError {
                    debugPrint(error.localizedDescription)
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    //MARK: Private Method
    
    private func storeData(data: Data, to url: URL) throws {
        guard let cacheURL = cacheURL else { return }
        
        let fileManager = FileManager.default
        let fileName = fileNameFrom(url: url)
        let fileURL = cacheURL.appendingPathComponent(fileName)
        
        if fileManager.fileExists(atPath: fileURL.path) == false {
            try data.write(to: fileURL)
        }
    }
    
    private func loadData(url: URL, shouldCacheinMemory: Bool) throws -> Data? {
        guard let cacheURL = cacheURL else {
            return nil
        }
        
        let fileName = fileNameFrom(url: url)
        let fileURL = cacheURL.appendingPathComponent(fileName)
        let data = try Data(contentsOf: fileURL)
        
        if shouldCacheinMemory {
            cache.setObject(data as NSData, forKey: fileName as NSString)
        }
    
        return data
    }
    
    private func fileNameFrom(url: URL) -> String {
        return url.absoluteString.replacingOccurrences(of: "/", with: "-").replacingOccurrences(of: ":", with: ".")
    }
    
}
