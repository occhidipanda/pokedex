//
//  Pokemon.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import UIKit

struct Pokemon: Decodable {
    var id: Int?
    var order: Int?
    var name: String?
    var height: Double?
    var weight: Double?
    var abilities: [Ability]?
    var types: [Types]?
    var stats: [Stat]?
    var moves: [Move]?
    var species: ListItem?
    var sprites: Sprites?
    
    var frontImage: UIImage? = nil
    var backImage: UIImage? = nil
    var info: Species? = nil
    
    private enum CodingKeys: String, CodingKey {
        case id, order, name, height, weight, types, stats, moves, sprites, abilities
    }
    
}

struct Ability: Decodable {
    let ability: ListItem?
}

struct Types: Decodable {
    var slot: Int?
    var type: ListItem?
    
    var color: UIColor? {
        return PokeTypeColor(rawValue: type?.name ?? "unknown")?.color
    }
    
}

struct Stat: Decodable {
   
    let baseStat: Int?
    let stat: ListItem?
    
    private enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat", stat
    }
    
}

struct Move: Decodable {
    let move: ListItem?
}

struct Sprites: Decodable {
    let back_default: URL?
    let front_default: URL?
}
