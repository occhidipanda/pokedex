//
//  PokemonListItem.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import Foundation

struct Result: Decodable {
    let count: Int?
    let next: URL?
    let previous: URL?
    let results: [ListItem]?
}

struct ListItem: Decodable {
    let name: String?
    let url: URL?
}
