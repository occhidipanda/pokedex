//
//  HttpResponse.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import UIKit

enum ResponseStatus : Int {
    case timeout = -1, jsonParseError = -2, genericError = 0, success = 200
}

struct HttpResponse {
    
    private(set) var httpStatus : Int!
    private(set) var rawBody : Data?
    
    var responseStatus : ResponseStatus?{
        return ResponseStatus(rawValue: httpStatus) ?? .genericError
    }
    
    var success : Bool {
        return httpStatus == 200
    }
    
    init(status : Int) {
        self.httpStatus = status
    }
    
    init(status : Int, body : Data) {
        self.httpStatus = status
        self.rawBody = body
    }
    
    func getData<T : Decodable>(of type: T.Type) throws -> T? {
        guard let body = rawBody else {
            return nil
        }
        let jsonDecoder = JSONDecoder()
        return try jsonDecoder.decode(type, from: body)
    }

}
