//
//  PokeType.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import UIKit

enum PokeTypeColor: String {
    case normal, fighting, flying, poison, ground, rock, bug, ghost, steel, fire, water, grass, electric, psychic, ice, dragon, dark, fairy, unknown, shadow
        
    var color: UIColor {
        switch self {
        case .normal:
            return .init(red: 168/255, green: 168/255, blue: 120/255, alpha: 1)
        case .fighting:
            return .init(red: 119/255, green: 48/255, blue: 40/255, alpha: 1)
        case .flying:
            return .init(red: 168/255, green: 144/255, blue: 240/255, alpha: 1)
        case .poison:
            return .init(red: 159/255, green: 64/255, blue: 160/255, alpha: 1)
        case .ground:
            return .init(red: 224/255, green: 192/255, blue: 104/255, alpha: 1)
        case .rock:
            return .init(red: 184/255, green: 160/255, blue: 56/255, alpha: 1)
        case .bug:
            return .init(red: 167/255, green: 184/255, blue: 32/255, alpha: 1)
        case .ghost, .shadow:
            return .init(red: 112/255, green: 87/255, blue: 152/255, alpha: 1)
        case .steel:
            return .init(red: 184/255, green: 184/255, blue: 208/255, alpha: 1)
        case .fire:
            return .init(red: 240/255, green: 128/255, blue: 48/255, alpha: 1)
        case .water:
            return .init(red: 104/255, green: 144/255, blue: 240/255, alpha: 1)
        case .grass:
            return .init(red: 120/255, green: 200/255, blue: 79/255, alpha: 1)
        case .electric:
            return .init(red: 248/255, green: 208/255, blue: 48/255, alpha: 1)
        case .psychic:
            return .init(red: 248/255, green: 88/255, blue: 136/255, alpha: 1)
        case .ice:
            return .init(red: 152/255, green: 216/255, blue: 216/255, alpha: 1)
        case .dragon:
            return .init(red: 112/255, green: 56/255, blue: 248/255, alpha: 1)
        case .dark:
            return .init(red: 111/255, green: 88/255, blue: 72/255, alpha: 1)
        case .fairy:
            return .init(red: 240/255, green: 182/255, blue: 188/255, alpha: 1)
        case .unknown:
            return .init(red: 106/255, green: 165/255, blue: 151/255, alpha: 1)

        }
    }
}
