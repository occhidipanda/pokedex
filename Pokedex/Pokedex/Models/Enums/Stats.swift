//
//  Stats.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import UIKit

enum Stats: String {
    case hp, attack, defense, specialAttack = "special-attack", specialDefense = "special-defense", speed, total
    
    var title: String{
        switch self {
        case .hp, .attack, .defense, .speed, .total:
            return self.rawValue
        case .specialAttack:
            return "sp. atk"
        case .specialDefense:
            return "sp. def"
        }
    }
    
    var color: UIColor? {
        switch self {
        case .hp:
            return .init(red: 1, green: 89/255, blue: 89/255, alpha: 1)
        case .attack:
            return .init(red: 245/255, green: 172/255, blue: 120/255, alpha: 1)
        case .defense:
            return .init(red: 250/255, green: 224/255, blue: 120/255, alpha: 1)
        case .specialAttack:
            return .init(red: 157/255, green: 184/255, blue: 245/255, alpha: 1)
        case .specialDefense:
            return .init(red: 167/255, green: 220/255, blue: 141/255, alpha: 1)
        case .speed:
            return .init(red: 250/255, green:147/255, blue: 178/255, alpha: 1)
        case .total:
            return nil
        }
    
    }

}
