//
//  Species.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Foundation

struct Species: Decodable {
    let habitat: ListItem?
    let eggGroups: [ListItem]?
    let growthRate: ListItem?
    let shape: ListItem?
    
    private enum CodingKeys: String, CodingKey {
        case habitat, eggGroups = "egg_groups", growthRate = "growth_rate", shape
    }
}
