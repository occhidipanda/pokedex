//
//  Reachability.swift
//  Pokedex
//
//  Created by Tiziano on 27/01/21.
//

import Foundation
import SystemConfiguration

protocol ReachabilityDelegate {
    func statusDidChanged(flag: SCNetworkReachabilityFlags)
}

class Reachability: NSObject {

    private var reachability: SCNetworkReachability?
    private let queue = DispatchQueue.main
    private var currentReachabilityFlags: SCNetworkReachabilityFlags?
    private var delegate: ReachabilityDelegate?
    private var isListening = false
    
    override init() {
        super.init()
        var address = sockaddr_in()
        address.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        address.sin_family = sa_family_t(AF_INET)
        reachability = withUnsafePointer(to: &address, { pointer in
            return pointer.withMemoryRebound(to: sockaddr.self, capacity: MemoryLayout<sockaddr>.size) {
                return SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    func isConnectedToNetwork() -> Bool {
        guard let r = reachability else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(r, &flags) {
            return false
        }
        
        currentReachabilityFlags = flags
        
        return flags.contains(.reachable)
    }
    
    func startMonitoring(delegate: ReachabilityDelegate) {
        
        guard !isListening else { return }
        
        self.delegate = delegate
        
        guard let reachability = reachability else { return }
        
        var context = SCNetworkReachabilityContext(version: 0, info: nil, retain: nil, release: nil, copyDescription: nil)
        
        context.info = UnsafeMutableRawPointer(Unmanaged<Reachability>.passUnretained(self).toOpaque())
        let callbackClosure: SCNetworkReachabilityCallBack? = {
            (reachability:SCNetworkReachability, flags: SCNetworkReachabilityFlags, info: UnsafeMutableRawPointer?) in
            guard let info = info else { return }
            let handler = Unmanaged<Reachability>.fromOpaque(info).takeUnretainedValue()
            DispatchQueue.main.async {
                handler.checkReachability(flags: flags)
            }
        }
        
        if !SCNetworkReachabilitySetCallback(reachability, callbackClosure, &context) || !SCNetworkReachabilitySetDispatchQueue(reachability, queue) {
            return
        }
        
        queue.async {
            self.currentReachabilityFlags = nil
            var flags = SCNetworkReachabilityFlags()
            SCNetworkReachabilityGetFlags(reachability, &flags)
            self.checkReachability(flags: flags)
        }
        
        isListening = true
    }
    
    func stopMonitoring() {
        guard isListening,
            let reachability = reachability
            else { return }
        SCNetworkReachabilitySetCallback(reachability, nil, nil)
        SCNetworkReachabilitySetDispatchQueue(reachability, nil)
        isListening = false
    }

    private func checkReachability(flags: SCNetworkReachabilityFlags) {
        if currentReachabilityFlags != flags {
            currentReachabilityFlags = flags
            delegate?.statusDidChanged(flag: flags)
        }
    }
}
