//
//  Networking.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import Foundation

class NetworkManager: NSObject {
    
    static let shared: NetworkManager = NetworkManager()

    private let queue: OperationQueue = {
        let _queue = OperationQueue()
        _queue.name = "tasks"
        _queue.maxConcurrentOperationCount = 3
        return _queue
    }()
    
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }()
        
    private override init(){
        super.init()
    }
    
    func addOperation(_ operation: BaseOperation) {
        queue.addOperation(operation)
    }
    
    func addOperations(_ operations: [BaseOperation], completion: (() -> Void)?) {
        DispatchQueue.global(qos: .background).async {
            self.queue.addOperations(operations, waitUntilFinished: true)
            DispatchQueue.main.async {
                completion?()
            }
        }
    }
    
    func cancelAll() {
        queue.cancelAllOperations()
    }
}


