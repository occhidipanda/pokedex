//
//  AsynchriniusOperation.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import UIKit

@objc private enum OperationState: Int {
    case ready
    case executing
    case finished
}

enum HTTPMethod: String {
    case connect
    case delete
    case get
    case head
    case options
    case patch
    case post
    case put
    case trace
}

typealias BaseOperationCompletionBlock = ((HttpResponse) -> Void)

class BaseOperation: Operation {
    
    private let baseUrl = URL(string: "https://pokeapi.co/api/v2/")!
    private var cacheManager: CacheManager?
    private var session: URLSession?
    var task: URLSessionDataTask?
    var completion: BaseOperationCompletionBlock?
    
    private let stateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! + ".rw.state", attributes: .concurrent)
    private var rawState: OperationState = .ready
    
    @objc private dynamic var state: OperationState {
        get { return stateQueue.sync { rawState } }
        set { stateQueue.sync(flags: .barrier) { rawState = newValue } }
    }
    
    override var isReady: Bool { return state == .ready && super.isReady }
    final override var isExecuting: Bool { return state == .executing }
    final override var isFinished: Bool { return state == .finished }
    
    init(session: URLSession, cacheManager: CacheManager = CacheManager.shared) {
        super.init()
        self.session = session
        self.cacheManager = cacheManager
    }
    
    //MARK: KVO for dependent properties
    override class func keyPathsForValuesAffectingValue(forKey key: String) -> Set<String> {
        if ["isReady", "isFinished", "isExecuting"].contains(key) {
            return [#keyPath(state)]
        }
        
        return super.keyPathsForValuesAffectingValue(forKey: key)
    }
    
    //MARK: BusinessLogin Method
    private func createDataTask(_ url : URL?, method : HTTPMethod, body: Data?, headers : [String : String]) -> URLSessionDataTask? {
        
        guard let url = url else {
            DispatchQueue.main.async {
                self.completion?(HttpResponse(status: 0))
            }
            return nil
        }
        
        var request = URLRequest(url: url)
        
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = 15
        
        if method != .get {
            request.httpMethod = method.rawValue
            request.httpBody = body
        }
        
        return session?.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
            DispatchQueue.main.async {
                if let httpResponse = response as? HTTPURLResponse, let data = data {
                    #if DEBUG
                    request.toString()
                    debugPrint("Status \(httpResponse.statusCode)")
//                  print("Response body \(String(data: data, encoding: .utf8) ?? "NO RESPONSE BODY")")
                    debugPrint("REST CALL END****************************************")
                    #endif
                    do {
                        try self?.cacheManager?.cacheJSONData(data, with: url)
                    }catch{
                        #if DEBUG
                        debugPrint(error.localizedDescription)
                        #endif
                    }
                    self?.completion?(HttpResponse(status: httpResponse.statusCode, body: data))
                }else{
                    #if DEBUG
                    debugPrint("REST CALL RESPONSE ERROR****************************************")
                    #endif
                    self?.completion?(HttpResponse(status: -1))
                }
                self?.finish()
            }
        })
    }
    
    func get(path: String, headers: [String : String], params : [URLQueryItem]) -> URLSessionDataTask? {
        guard var urlComponents = URLComponents(url: baseUrl.appendingPathComponent(path), resolvingAgainstBaseURL: false) else {
            return nil
        }
        urlComponents.queryItems = params
        return createDataTask(urlComponents.url, method: .get, body: nil, headers: headers)
        
    }
    
    //MARK: Operation Methods
    final override func start() {
        if isCancelled {
            finish()
            return
        }
        
        state = .executing
        
        main()
    }
    
    override func main() {
        guard let task = task, let url = task.currentRequest?.url else {
            finish()
            return
        }
        do {
            if let cacheData = try cacheManager?.getCachedData(url: url) {
                DispatchQueue.main.async {
                    self.completion?(HttpResponse(status: 200, body: cacheData))
                }
                finish()
                return
            }
        }catch{
            #if DEBUG
            debugPrint(error.localizedDescription)
            #endif
        }
        task.resume()
    }
    
    final func finish() {
        if !isFinished { state = .finished }
    }
}

fileprivate extension String {
    var data: Data {
        return self.data(using: .utf8) ?? Data()
    }
}

extension URLRequest {
    func toString() {
        
        debugPrint("REST CALL ****************************************")
        debugPrint("Url \(url?.absoluteString ?? "NULL!")")
        debugPrint("Method \(httpMethod ?? "NULL!")")
        
        if let body = httpBody, let str = String(data: body, encoding: .utf8) {
            debugPrint(str)
        }
        
        if let httpHeader = allHTTPHeaderFields {
            for (key,value) in httpHeader {
                debugPrint(key + " : " + value)
            }
        }
    }
}
