//
//  PokemonOperation.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Foundation

class PokemonOperation: BaseOperation {
    
    let basePath = "pokemon"

    init(session: URLSession, pokemonName name: String, completionBlock: BaseOperationCompletionBlock? = nil) {
        super.init(session: session)
        self.completion = completionBlock
        task = get(path: "\(basePath)/\(name)", headers: [:], params: [])
    }
    
}
