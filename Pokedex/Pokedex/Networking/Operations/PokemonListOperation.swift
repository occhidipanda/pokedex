//
//  PokemonListOperation.swift
//  Pokedex
//
//  Created by Tiziano on 24/01/21.
//

import Foundation

class PokemonListOperation: BaseOperation {
    
    let basePath = "pokemon"
    
    init(session: URLSession, limit: Int, offset: Int, completionBlock: BaseOperationCompletionBlock? = nil) {
        super.init(session: session)
        
        self.completion = completionBlock
    
        var params: [URLQueryItem] = []
        params.append(.init(name: "limit", value: String(limit)))
        params.append(.init(name: "offset", value: String(offset)))
        task = get(path: basePath, headers: [:], params: params)
    
    }
    
    init(session: URLSession, nextURL: URL, completionBlock: BaseOperationCompletionBlock? = nil) {
        super.init(session: session)
        
        self.completion = completionBlock
        
        if let urlComnponents = URLComponents(url: nextURL, resolvingAgainstBaseURL: false) {
            task = get(path: basePath, headers: [:], params: urlComnponents.queryItems ?? [])
        }
        
    }
        
}
