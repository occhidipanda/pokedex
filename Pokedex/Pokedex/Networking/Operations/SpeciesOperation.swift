//
//  SpeciesOperation.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Foundation


class SpeciesOperation: BaseOperation {
    
    let basePath = "pokemon-species"

    init(session: URLSession, id: String, completionBlock: BaseOperationCompletionBlock? = nil) {
        super.init(session: session)
        self.completion = completionBlock
        task = get(path: "\(basePath)/\(id)", headers: [:], params: [])
    }
    
}
