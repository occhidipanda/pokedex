//
//  PokemonMovesView.swift
//  Pokedex
//
//  Created by Tiziano on 27/01/21.
//

import Anchorage

class PokemonMovesView: UIView {
    
    lazy var collectionView: UICollectionView = {
        let layout = PokemonMovesCollectionViewLayout(insetBy: .init(top: 24, left: 24, bottom: safeAreaInsets.bottom + 24, right: 24))
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(PokemonMovesCell.self, forCellWithReuseIdentifier: PokemonMovesCell.reuseIdentifier)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
        configureConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureUI(){
        addSubview(collectionView)
    }
    
    func configureConstraints(){
        collectionView.verticalAnchors /==/ verticalAnchors
        collectionView.horizontalAnchors /==/ horizontalAnchors
    }

}
