//
//  PokemonMovesCell.swift
//  Pokedex
//
//  Created by Tiziano on 27/01/21.
//

import Anchorage

class PokemonMovesCell: UICollectionViewCell {
    
    static let reuseIdentifier = "PokemonMovesCell"
    
    let labelTitle: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 13, weight: .bold)
        label.numberOfLines = 2
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
        configureConstraints()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureUI(){
        clipsToBounds = false
        contentView.layer.cornerRadius = 15
        contentView.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
        contentView.addSubview(labelTitle)
    }
    
    private func configureConstraints(){
        labelTitle.verticalAnchors /==/ contentView.verticalAnchors
        labelTitle.horizontalAnchors /==/ contentView.horizontalAnchors + 16
    }
}
