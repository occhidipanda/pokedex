//
//  PokemonMovesCellViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 27/01/21.
//

import UIKit

class PokemonMovesCellViewModel: ItemViewModel {
    
    private(set) var move: Move
    
    var reuseIdentifier: String {
        return PokemonMovesCell.reuseIdentifier
    }
    
    init(item: Move) {
        self.move = item
    }

    func setup(_ cell: UICollectionViewCell, in collectionView: UICollectionView, at indexPath: IndexPath) {
        guard let cell = cell as? PokemonMovesCell else {
            return
        }
        cell.labelTitle.text = move.move?.name?.replacingOccurrences(of: "-", with: " ").capitalized
    }
}
