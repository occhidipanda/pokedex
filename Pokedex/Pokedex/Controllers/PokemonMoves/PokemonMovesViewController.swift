//
//  PokemonMovesViewController.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import UIKit

class PokemonMovesViewController: UIViewController {

    var viewModel: PokedexMovesViewModel?
    
    init(pokemon: Pokemon) {
        super.init(nibName: nil, bundle: nil)
        viewModel = PokedexMovesViewModel(pokemon: pokemon)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var _view: PokemonMovesView? {
        return view as? PokemonMovesView
    }
    
    override func loadView() {
        view = PokemonMovesView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel?.initMoves(collectionView: _view?.collectionView)
        
    }

}
