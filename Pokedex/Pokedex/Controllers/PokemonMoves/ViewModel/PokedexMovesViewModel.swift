//
//  PokedexMovesViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 27/01/21.
//

import UIKit

class PokedexMovesViewModel: NSObject {
    
    lazy var collectionViewDataSource = CollectionViewDataSource()
    
    private var pokemon: Pokemon
    
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
        super.init()
    }
    
    func initMoves(collectionView: UICollectionView?) {
        collectionView?.dataSource = collectionViewDataSource
        self.collectionViewDataSource.items.append(contentsOf: pokemon.moves?.map({PokemonMovesCellViewModel(item: $0)}) ?? [])

    }
}



