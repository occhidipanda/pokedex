//
//  PokedexDetailViewController.swift
//  Pokedex
//
//  Created by Tiziano on 25/01/21.
//

import UIKit

class PokedexDetailViewController: UIViewController {
    
    private var viewModel: PokedexDetailViewModel?
    private let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    
    var _view: PokedexDetailView? {
        return view as? PokedexDetailView
    }
    
    init(with pokemon: Pokemon) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = .init(with: pokemon)
        self.viewModel?.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = PokedexDetailView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let view = _view else {
            return
        }
        
        pageViewController.add(to: view.containerView, parent: self)

        viewModel?.configure(view)
        
        pageViewController.delegate = viewModel
        pageViewController.dataSource = viewModel
        
        view.buttonClose.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        view.segmentView.addTarget(self, action: #selector(segmentDidSwitch(_:)), for: .valueChanged)
        
        
    }

    
    //MARK: - UserInteraction
    
    @objc
    private func didTapCloseButton(){
        dismiss(animated: true)
    }
    
    @objc
    private func segmentDidSwitch(_ sender: UISegmentedControl){
        viewModel?.segmentDidSwitch(to: sender.selectedSegmentIndex)
    }
}


extension PokedexDetailViewController: PokedexDetailViewModelDelegate {
    func connectionDidChangeState(isConnected: Bool) {
        
    }
    
    func hideLoader() {
        _view?.hideLoader()
    }
    
    func showLoader() {
        _view?.showLoader()
    }
    
    func didSelectController(_ controller: UIViewController, forPageViewControllerDirection direction: UIPageViewController.NavigationDirection, animated: Bool, completion: ((Bool) -> Void)?) {
        pageViewController.setViewControllers([controller], direction: direction, animated: animated, completion: completion)
    }
    
    func didSelectPage(page: Int) {
        _view?.segmentView.selectedSegmentIndex = page
    }
}
