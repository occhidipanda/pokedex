//
//  PokedexDetailViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 25/01/21.
//

import UIKit
import Network

protocol PokedexDetailViewModelDelegate {
    func connectionDidChangeState(isConnected: Bool)
    func hideLoader()
    func showLoader()
    func didSelectPage(page: Int)
    func didSelectController(_ controller: UIViewController, forPageViewControllerDirection direction: UIPageViewController.NavigationDirection, animated: Bool, completion: ((Bool) -> Void)?)
}

enum MenuElements: String {
    case about, stats = "base stats", moves
}

class PokedexDetailViewModel: NSObject {
    
    private var pokemon: Pokemon
    private let menuElements: [MenuElements] = [.about, .stats, .moves]
    private let reachability = Reachability()
    private var currentMenuIndex: Int = 0
    var delegate: PokedexDetailViewModelDelegate?
    
    
    init(with pokemon: Pokemon) {
        self.pokemon = pokemon
        super.init()
    }
    
    func configure(_ view: PokedexDetailView) {

        view.labelTitle.text = pokemon.name?.capitalized
        view.labelNumber.text = "#\(pokemon.id ?? 0)"

        view.backgroundColor = pokemon.types?.first?.color
        
        for (index, element) in menuElements.enumerated() {
            view.segmentView.insertSegment(withTitle: element.rawValue.capitalized, at: index, animated: false)
        }
        
        if #available(iOS 13.0, *) {
            view.segmentView.selectedSegmentTintColor = pokemon.types?.first?.color
            view.segmentView.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)], for: .selected)
            view.segmentView.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .medium)], for: .normal)
        }
        
        if pokemon.types?.count == 1 {
            view.labelSecondType.superview?.isHidden = true
            view.labelFirstType.text = pokemon.types?.first?.type?.name?.uppercased()
        }else if pokemon.types?.count == 2 {
            view.labelFirstType.text = pokemon.types?[0].type?.name?.uppercased()
            view.labelSecondType.text = pokemon.types?[1].type?.name?.uppercased()
        }else{
            view.labelFirstType.superview?.isHidden = true
            view.labelSecondType.superview?.isHidden = true
        }

        if let img = pokemon.frontImage {
            view.imageViewPokemon.image = img
        } else {
            guard let url = pokemon.sprites?.front_default else {
                return
            }
            if let imageData = try? CacheManager.shared.getCachedData(url: url), let image = UIImage(data: imageData) {
                pokemon.frontImage = image
                view.imageViewPokemon.image = image
            } else {
                DispatchQueue.global(qos: .background).async { [weak self] in
                    do {
                        let data = try Data(contentsOf: url)
                        try? CacheManager.shared.cacheImage(imageData: data, with: url)
                        
                        let image = UIImage(data: data)
                        DispatchQueue.main.async {
                            self?.pokemon.frontImage = image
                            view.imageViewPokemon.image = image
                        }
                    }catch{
                        print(error)
                    }
                }
            }
        }
        
        self.delegate?.didSelectController(UIViewController(), forPageViewControllerDirection: .forward, animated: false, completion: nil)
        
        let operation = SpeciesOperation(session: NetworkManager.shared.session, id: String(pokemon.id ?? -1)) { [weak self] (response) in
            guard let self = self else {return}
            self.delegate?.hideLoader()
            
            if let species = try? response.getData(of: Species.self) {
                self.pokemon.info = species
            }
            
            self.delegate?.didSelectController(PokemonInfoViewController(pokemon: self.pokemon), forPageViewControllerDirection: .reverse, animated: true, completion: { (f) in
                view.segmentView.selectedSegmentIndex = 0
            })
        }
        NetworkManager.shared.addOperation(operation)
        delegate?.showLoader()
    }
    
    func segmentDidSwitch(to index: Int){
        
        let direction: UIPageViewController.NavigationDirection = currentMenuIndex < index ? .forward : .reverse
        var controller: UIViewController
        
        if index == 0 {
            controller = PokemonInfoViewController(pokemon: pokemon)
        }else if index == 1 {
            controller = PokemonStatsViewController(pokemon: pokemon)
        }else{
            controller = PokemonMovesViewController(pokemon: pokemon)
        }
        
        currentMenuIndex = index
        
        delegate?.didSelectController(controller, forPageViewControllerDirection: direction, animated: true, completion: nil)
    }
}

//MARK: - UIPageViewControllerDelegate, UIPageViewControllerDataSource
extension PokedexDetailViewModel: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController is PokemonInfoViewController {
            return nil
        }else if viewController is PokemonStatsViewController {
            return PokemonInfoViewController(pokemon: pokemon)
        }else if viewController is PokemonMovesViewController {
            return PokemonStatsViewController(pokemon: pokemon)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController is PokemonInfoViewController {
            return PokemonStatsViewController(pokemon: pokemon)
        }else if viewController is PokemonStatsViewController {
            return PokemonMovesViewController(pokemon: pokemon)
        }else{
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let controller = pageViewController.viewControllers?.first, completed else { return }
        
        if  controller is PokemonInfoViewController {
            currentMenuIndex = 0
        }else if controller is PokemonStatsViewController {
            currentMenuIndex = 1
        }else{
            currentMenuIndex = 2
        }
        
        delegate?.didSelectPage(page: currentMenuIndex)
    }
}
