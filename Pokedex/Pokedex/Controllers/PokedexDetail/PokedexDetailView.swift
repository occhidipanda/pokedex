//
//  PokedexDetailView.swift
//  Pokedex
//
//  Created by Tiziano on 25/01/21.
//

import Anchorage

class PokedexDetailView: UIView {
    
    
    let labelNumber: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 25, weight: .black)
        label.textColor = .white
        return label
    }()
    
    let labelTitle: UILabel = {
        let label = UILabel()
        label.setContentHuggingPriority(.required, for: .vertical)
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        label.textColor = .white
        return label
    }()
    
    let buttonClose: UIButton = {
        let button = UIButton()
        button.imageEdgeInsets = .init(top: 5, left: 5, bottom: 5, right: 5)
        button.setImage(UIImage(named: "close"), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    let labelFirstType: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 13, weight: .semibold)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let labelSecondType: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 13, weight: .semibold)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    private lazy var stackViewTypes: UIStackView = {
        let stackView = UIStackView()
        
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        stackView.alignment = .fill
        stackView.axis = .horizontal
        
        let view1 = UIView()
        view1.addSubview(labelFirstType)
        view1.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        view1.layer.cornerRadius = 5
        view1.clipsToBounds = true
        
        labelFirstType.horizontalAnchors /==/ view1.horizontalAnchors + 8
        labelFirstType.verticalAnchors /==/ view1.verticalAnchors
        
        let view2 = UIView()
        view2.addSubview(labelSecondType)
        view2.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        view2.layer.cornerRadius = 5
        view2.clipsToBounds = true
        
        labelSecondType.horizontalAnchors /==/ view2.horizontalAnchors + 8
        labelSecondType.verticalAnchors /==/ view2.verticalAnchors
        
        stackView.addArrangedSubview(view1)
        stackView.addArrangedSubview(view2)
        
        return stackView
    }()
    
    let imageViewPokemon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let segmentView: UISegmentedControl = UISegmentedControl()
    
    let containerView: UIView = UIView()
    
    private let imageViewLoader: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        
        imageView.contentMode = .scaleAspectFit
        var images = [UIImage]()
        for i in 1..<9 {
            images.append(UIImage(named: "pokeball-\(i)") ?? UIImage())
        }
        imageView.animationImages = images
        imageView.animationDuration = 1
        
        return imageView
    }()
    
    lazy var loadingView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = false
        view.addSubview(imageViewLoader)
        imageViewLoader.centerAnchors /==/ view.centerAnchors
        return view
    }()
    
    private lazy var viewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.setContentHuggingPriority(.defaultLow, for: .vertical)
        
        view.addSubview(segmentView)
        view.addSubview(containerView)
        view.addSubview(loadingView)
    
        segmentView.horizontalAnchors /==/ view.horizontalAnchors + 24
        segmentView.topAnchor /==/ view.topAnchor + 40
        
        containerView.topAnchor /==/ segmentView.bottomAnchor
        containerView.horizontalAnchors /==/ view.horizontalAnchors
        containerView.bottomAnchor /==/ view.bottomAnchor - 20
        
        loadingView.verticalAnchors /==/ containerView.verticalAnchors
        loadingView.horizontalAnchors /==/ containerView.horizontalAnchors
        
        return view
    }()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
        configureConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureUI(){
        backgroundColor = .white
        addSubview(buttonClose)
        addSubview(labelTitle)
        addSubview(labelNumber)
        addSubview(stackViewTypes)
        addSubview(viewContainer)
        addSubview(imageViewPokemon)
    }
    
    private func configureConstraints(){
        
        labelNumber.topAnchor /==/ safeAreaLayoutGuide.topAnchor + 24
        labelNumber.leftAnchor /==/ safeAreaLayoutGuide.leftAnchor + 24
        labelNumber.heightAnchor /==/ 35
        
        buttonClose.centerYAnchor /==/ labelNumber.centerYAnchor
        buttonClose.rightAnchor /==/ rightAnchor - 24
        buttonClose.heightAnchor /==/ 35
        buttonClose.widthAnchor /==/ 35
        
        labelTitle.topAnchor /==/ labelNumber.bottomAnchor
        labelTitle.leftAnchor /==/ labelNumber.leftAnchor
        
        stackViewTypes.topAnchor /==/ labelTitle.bottomAnchor + 8
        stackViewTypes.leftAnchor /==/ labelTitle.leftAnchor
        stackViewTypes.heightAnchor /==/ 25
        
        imageViewPokemon.heightAnchor /==/ 100
        imageViewPokemon.rightAnchor /==/ buttonClose.rightAnchor
        imageViewPokemon.bottomAnchor /==/ viewContainer.topAnchor + 40
        
        viewContainer.topAnchor /==/ stackViewTypes.bottomAnchor + 16
        viewContainer.horizontalAnchors /==/ horizontalAnchors
        viewContainer.bottomAnchor /==/ bottomAnchor + 20
        
    }
    
    func showLoader(){
        imageViewLoader.startAnimating()
        UIView.animate(withDuration: 0.3) {
            self.loadingView.alpha = 1
        }
    }
    
    func hideLoader(){
        imageViewLoader.stopAnimating()
        UIView.animate(withDuration: 0.3) {
            self.loadingView.alpha = 0
        }
    }
    
    func setViewConnection(hidden: Bool){
        UIView.animate(withDuration: 0.3) {
            
        }
    }
    
}
