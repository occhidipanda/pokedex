//
//  PokedexCollectionViewLayout.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import UIKit

class PokedexCollectionViewLayout: UICollectionViewFlowLayout {
    
    init(insetBy edge: UIEdgeInsets) {
        super.init()
        sectionInset = edge
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        scrollDirection = .vertical
        minimumLineSpacing = 8
        minimumInteritemSpacing = 8
        
        guard let collectionView = collectionView else { return }
    
        let availableWidth = collectionView.bounds.inset(by: sectionInset).width
        let numberOfColumns = UIDevice.current.userInterfaceIdiom == .pad ? 4 : 2
        let availableColumnsWidth = availableWidth-(minimumInteritemSpacing * CGFloat((numberOfColumns-1)))
        let columnWidth = availableColumnsWidth/CGFloat(numberOfColumns)
        let columnHeight: CGFloat = 135
        
        itemSize = CGSize(width: columnWidth, height: columnHeight)

    }
    
}
