//
//  PokedexCellViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 25/01/21.
//

import UIKit

class PokedexCellViewModel: ItemViewModel {

    private(set) var pokemon: Pokemon
    
    var reuseIdentifier: String {
        return PokedexCell.reuseIdentifier
    }
    
    init(item: Pokemon) {
        self.pokemon = item
    }

    func setup(_ cell: UICollectionViewCell, in collectionView: UICollectionView, at indexPath: IndexPath) {
        guard let cell = cell as? PokedexCell else {
            return
        }
        cell.labelTitle.text = pokemon.name?.capitalized
        cell.labelNumber.text = "#\(pokemon.id ?? 0)"
        if let type = pokemon.types?.first(where: {$0.slot == 1}) {
            cell.contentView.backgroundColor = PokeTypeColor(rawValue: type.type?.name ?? "unknown")?.color ?? PokeTypeColor.unknown.color
        }else{
            cell.contentView.backgroundColor = PokeTypeColor.unknown.color
        }
        
        cell.labelFirstType.isHidden = true
        cell.labelSecondType.isHidden = true
        
        if let types = pokemon.types {
            for (i, t) in types.enumerated() {
                if i == 0 {
                    cell.labelFirstType.isHidden = false
                    cell.labelFirstType.text = t.type?.name?.uppercased()
                }else if i == 1 {
                    cell.labelSecondType.isHidden = false
                    cell.labelSecondType.text = t.type?.name?.uppercased()
                }else{
                    break
                }
            }
        }
        
        if let img = pokemon.frontImage {
            cell.imageView.image = img
        }else{
            guard let url = pokemon.sprites?.front_default else {
                return
            }
        
            if let imageData = try? CacheManager.shared.getCachedData(url: url), let image = UIImage(data: imageData) {
                pokemon.frontImage = image
                cell.imageView.image = image
            }else{
                cell.imageView.image = nil
                DispatchQueue.global(qos: .background).async { [weak self] in
                    do {
                        let data = try Data(contentsOf: url)
                        try? CacheManager.shared.cacheImage(imageData: data, with: url)
                        
                        let image = UIImage(data: data)
                        DispatchQueue.main.async {
                            self?.pokemon.frontImage = image
                            collectionView.reloadItems(at: [indexPath])
                        }
                    }catch{
                        print(error)
                    }
                }
            }
        }
    }
}
