//
//  PokedexCollectionViewCell.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import Anchorage

class PokedexCell: UICollectionViewCell {
    
    static let reuseIdentifier = "PokedexCollectionViewCell"
    
    let labelTitle: UILabel = {
        let label = UILabel()
        
        label.textColor = .white
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16, weight: .bold)
        
        return label
    }()
    
    let labelNumber: UILabel = {
        let label = UILabel()
        
        label.textColor = .white
        label.textAlignment = .right
        label.font = .systemFont(ofSize: 11, weight: .black)
        
        return label
    }()
    
    let labelFirstType: UILabel = {
        let label = UILabel()
        
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.textColor = .white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 10, weight: .semibold)
        label.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        
        return label
    }()
    
    let labelSecondType: UILabel = {
        let label = UILabel()
        
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.textColor = .white
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 10, weight: .semibold)
        label.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }()
    
    lazy var stackViewTypes: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [labelFirstType, labelSecondType])
        
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        stackView.alignment = .fill
        stackView.axis = .vertical
        
        labelFirstType.heightAnchor /==/ 20
        labelSecondType.heightAnchor /==/ 20
        
        return stackView
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
        configureConstraints()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelNumber.text = ""
        labelNumber.text = ""
        imageView.image = UIImage()
        contentView.backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureUI(){
        clipsToBounds = false
        contentView.layer.cornerRadius = 15

        contentView.addSubview(labelNumber)
        contentView.addSubview(labelTitle)
        contentView.addSubview(stackViewTypes)
        contentView.addSubview(imageView)
    }
    
    private func configureConstraints(){
        
        labelNumber.topAnchor /==/ contentView.topAnchor + 16
        labelNumber.horizontalAnchors /==/ contentView.horizontalAnchors + 16
    
        labelTitle.topAnchor /==/ labelNumber.bottomAnchor
        labelTitle.horizontalAnchors /==/ contentView.horizontalAnchors + 16
        
        imageView.topAnchor /==/ labelTitle.bottomAnchor
        imageView.rightAnchor /==/ contentView.rightAnchor - 16
        imageView.bottomAnchor /==/ contentView.bottomAnchor - 16
        imageView.leftAnchor /==/ contentView.centerXAnchor + 8

        stackViewTypes.centerYAnchor /==/ imageView.centerYAnchor
        stackViewTypes.leftAnchor /==/ contentView.leftAnchor + 16
        stackViewTypes.rightAnchor /==/ contentView.centerXAnchor
        
    }
    
}
