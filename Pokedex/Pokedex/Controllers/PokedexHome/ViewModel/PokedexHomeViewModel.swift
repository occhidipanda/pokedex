//
//  PokedexHomeViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import UIKit
import SystemConfiguration

protocol PokedexHomeViewModelDelegate {
    func reload()
    func showLoader()
    func hideLoader()
    func connectionDidChangeState(isConnected: Bool)
    func setConnectionView(hidden: Bool)
    func didSelected(pokemon: Pokemon)
}

class PokedexHomeViewModel: NSObject {
    
    var limit: Int {
        return UIDevice.current.userInterfaceIdiom == .pad ? 50 : 20
    }
    
    private let reachability = Reachability()
    private var pokemonResult: Result?
    var delegate: PokedexHomeViewModelDelegate?
    var isUpdateRequired: Bool = true
    var isUpdating: Bool = false
    
    override init() {
        super.init()
        reachability.startMonitoring(delegate: self)
    }
    
    lazy var collectionViewDataSource: CollectionViewDataSource = {
        let dataSource = CollectionViewDataSource()
        dataSource.delegate = self
        return dataSource
    }()
    
    func reloadIfNeeded() {
        guard pokemonResult == nil else{
            
            return
        }
        
        collectionViewDataSource.items.removeAll()
        
        let operation = PokemonListOperation(session: NetworkManager.shared.session, limit: limit, offset: 0) { [weak self] (result) in
            do{
                guard let pokemonList = try result.getData(of: Result.self) else {
                    self?.delegate?.hideLoader()
                    self?.delegate?.setConnectionView(hidden: false)
                    return
                }
                
                self?.pokemonResult = pokemonList
                
                var ops = [PokemonOperation]()
                var pokemons = [Pokemon]()
                
                for  item in self?.pokemonResult?.results ?? [] {
                    let operation = PokemonOperation(session: NetworkManager.shared.session, pokemonName: item.name ?? "") { (response) in
                        guard let poke = try? response.getData(of: Pokemon.self) else {
                            return
                        }
                        pokemons.append(poke)
                    }
                    ops.append(operation)
                }
                
                NetworkManager.shared.addOperations(ops) {
                    self?.delegate?.hideLoader()
                    self?.delegate?.setConnectionView(hidden: true)
                    let sorted = pokemons.sorted(by: {($0.id ?? 0) < ($1.id ?? 1)})
                    self?.collectionViewDataSource.items.append(contentsOf: sorted.map({PokedexCellViewModel(item: $0)}))
                    self?.delegate?.reload()
                }
                
            }catch{
                print(error.localizedDescription)
            }
        }
        
        NetworkManager.shared.addOperation(operation)
        delegate?.showLoader()
    }
    
    func initPokemonList(collectionView: UICollectionView?) {
        collectionView?.dataSource = collectionViewDataSource
        collectionView?.delegate = self
        
        

        reloadIfNeeded()
    }
}

extension PokedexHomeViewModel: ReachabilityDelegate {
    func statusDidChanged(flag: SCNetworkReachabilityFlags) {
        delegate?.connectionDidChangeState(isConnected: flag.contains(.reachable))
    }
}

//MARK: - UICollectionViewDelegate

extension PokedexHomeViewModel: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let pokemon = (collectionViewDataSource.items[indexPath.row] as? PokedexCellViewModel)?.pokemon else {
            return
        }
        delegate?.didSelected(pokemon: pokemon)
    }
}

//MARK: - PagingDelegate

extension PokedexHomeViewModel: PagingDelegate{
    
    func shouldUpdateData() {
        
        guard let nextURL = pokemonResult?.next else{
            isUpdateRequired = false
            return
        }
        isUpdating = true
        
        let operation = PokemonListOperation(session: NetworkManager.shared.session, nextURL: nextURL) { [weak self] (result) in
            do{
                guard let pokemonList = try result.getData(of: Result.self) else{
                    self?.isUpdating = false
                    self?.delegate?.hideLoader()
                    return
                }
            
                self?.pokemonResult = pokemonList
                self?.isUpdateRequired = self?.pokemonResult?.next != nil
                
                var ops = [PokemonOperation]()
                var pokemons = [Pokemon]()
                
                for  item in self?.pokemonResult?.results ?? [] {
                    let operation = PokemonOperation(session: NetworkManager.shared.session, pokemonName: item.name ?? "") { (response) in
                        guard let poke = try? response.getData(of: Pokemon.self) else {
                            return
                        }
                        pokemons.append(poke)
                    }
                    
                    ops.append(operation)
                }
                
                NetworkManager.shared.addOperations(ops) {
                    self?.delegate?.hideLoader()
                    let sorted = pokemons.sorted(by: {($0.id ?? 0) < ($1.id ?? 1)})
                    self?.collectionViewDataSource.items.append(contentsOf: sorted.map({PokedexCellViewModel(item: $0)}))
                    self?.isUpdating = false
                    self?.delegate?.reload()
                }
                
            }catch{
                print(error.localizedDescription)
            }
        }
        
        NetworkManager.shared.addOperation(operation)
        delegate?.showLoader()
    }
    
}
 
