//
//  PokedexHomeViewController.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import UIKit

class PokedexHomeViewController: UIViewController {
    
    lazy var viewModel: PokedexHomeViewModel = {
        let viewModel = PokedexHomeViewModel()
        viewModel.delegate = self
        return viewModel
    }()
    
    var _view: PokedexHomeView? {
        return view as? PokedexHomeView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    
    override func loadView() {
        view = PokedexHomeView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.initPokemonList(collectionView: _view?.collectionView)
    }
    
}

extension PokedexHomeViewController: PokedexHomeViewModelDelegate {
    func setConnectionView(hidden: Bool) {
        _view?.setViewConnection(hidden: hidden)
    }
    
    func reload() {
        _view?.collectionView.reloadData()
    }
    
    func didSelected(pokemon: Pokemon) {
        let detailController = PokedexDetailViewController(with: pokemon)
        self.present(detailController, animated: true)
    }
    
    func showLoader(){
        _view?.showLoader()
    }
    
    func hideLoader(){
        _view?.hideLoader()
    }
    
    func connectionDidChangeState(isConnected: Bool){
        _view?.imageViewNoConnection.isHidden = isConnected
        if isConnected {
            viewModel.reloadIfNeeded()
        }
    }
}
