//
//  PokedexHomeView.swift
//  Pokedex
//
//  Created by Tiziano on 23/01/21.
//

import UIKit
import Anchorage

class PokedexHomeView: UIView {
    
    let labelTitle: UILabel = {
        let label = UILabel()
        label.text = "Pokedex"
        label.font = .systemFont(ofSize: 30, weight: .bold)
        label.textColor = .black
        return label
    }()
    
    let imageViewNoConnection: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "cloud-off"))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .systemRed
        return imageView
    }()
    
    private lazy var stackViewTitle: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [imageViewNoConnection, labelTitle])
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.spacing = 8
        
        imageViewNoConnection.heightAnchor /==/ 35
        imageViewNoConnection.widthAnchor /==/ 35
        
        return stackView
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = PokedexCollectionViewLayout(insetBy: .init(top: 24, left: 24, bottom: safeAreaInsets.bottom + 24, right: 24))
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(PokedexCell.self, forCellWithReuseIdentifier: PokedexCell.reuseIdentifier)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    private let viewNoConnection = NoConnectionView()
    
    private let imageViewLoader: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        imageView.contentMode = .scaleAspectFit
        
        var images = [UIImage]()
        for i in 1..<9 {
            images.append(UIImage(named: "pokeball-\(i)") ?? UIImage())
        }
        imageView.animationImages = images
        imageView.animationDuration = 1
        
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI(){
        backgroundColor = .white
        viewNoConnection.alpha = 0
        
        addSubview(stackViewTitle)
        addSubview(imageViewLoader)
        addSubview(collectionView)
        addSubview(viewNoConnection)
    }
    
    private func setupConstraints(){
        
        stackViewTitle.topAnchor /==/ safeAreaLayoutGuide.topAnchor + 24
        stackViewTitle.leftAnchor /==/ safeAreaLayoutGuide.leftAnchor + 24
        
        imageViewLoader.centerYAnchor /==/ labelTitle.centerYAnchor
        imageViewLoader.rightAnchor /==/ safeAreaLayoutGuide.rightAnchor - 24
        imageViewLoader.heightAnchor /==/ 35
        imageViewLoader.widthAnchor /==/ 35
        
        collectionView.topAnchor /==/ labelTitle.bottomAnchor
        collectionView.horizontalAnchors /==/ safeAreaLayoutGuide.horizontalAnchors
        collectionView.bottomAnchor /==/ bottomAnchor
        
        viewNoConnection.verticalAnchors /==/ collectionView.verticalAnchors
        viewNoConnection.horizontalAnchors /==/ collectionView.horizontalAnchors
        
    }
    
    func showLoader(){
        imageViewLoader.startAnimating()
        UIView.animate(withDuration: 0.3) {
            self.imageViewLoader.alpha = 1
        }
    }
    
    func hideLoader(){
        imageViewLoader.stopAnimating()
        UIView.animate(withDuration: 0.3) {
            self.imageViewLoader.alpha = 0
        }
    }
    
    func setViewConnection(hidden: Bool){
        UIView.animate(withDuration: 0.3) {
            self.viewNoConnection.alpha = hidden ? 0 : 1
        }
    }
    
}
