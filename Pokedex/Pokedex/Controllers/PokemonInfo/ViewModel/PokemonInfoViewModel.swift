//
//  PokemonInfoViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Foundation
import SystemConfiguration

protocol PokemonInfoViewModelDelegate {
    func connectionDidChangeState(isConnected: Bool)
}

class PokemonInfoViewModel: NSObject {
    
    private let kiloTolibsConvertionRatio: Double = 2.20
    private let meterTofeetConvertionRatio: Double = 3.28
    private let footToInchConvertionRatio: Double = 12
    private let reachability = Reachability()
    var delegate: PokemonInfoViewModelDelegate?
    private var pokemon: Pokemon
    
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
        super.init()
        reachability.startMonitoring(delegate: self)
    }
    
    func reloadIfNeeded(view: PokemonInfoView){
        guard pokemon.info == nil else{
            return
        }
        
        let operation = SpeciesOperation(session: NetworkManager.shared.session, id: String(pokemon.id ?? -1)) { [weak self] (response) in
            guard let self = self else {return}
            if let species = try? response.getData(of: Species.self) {
                self.pokemon.info = species
            }
            self.configure(view: view)
        }
        NetworkManager.shared.addOperation(operation)
    }
    
    func configure(view: PokemonInfoView){
        
        view.clearStackView()
        
        if let shape = pokemon.info?.shape?.name?.capitalized {
            view.addInfoRow(key: "Shape", value: shape)
        }
        
        let heightMeters = (pokemon.height ?? 0) / 10
        let heightFeet = heightMeters * meterTofeetConvertionRatio
        let heightInch = heightFeet.truncatingRemainder(dividingBy: 1) * footToInchConvertionRatio
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.decimalSeparator = "."
        
        let heightText =  "\(Int(heightFeet))'\(numberFormatter.string(from: NSNumber(value: heightInch)) ?? "N/D")\" (\(numberFormatter.string(from: NSNumber(value: heightMeters)) ?? "N/D") m)"
    
        view.addInfoRow(key: "Height", value: heightText)
    
        let weightKg = (pokemon.weight ?? 0) / 10
        let weightLibs = weightKg * kiloTolibsConvertionRatio
        
        let weightText = "\(numberFormatter.string(from: NSNumber(value: weightLibs)) ?? "N/D") lbs (\(numberFormatter.string(from: NSNumber(value: weightKg)) ?? "N/D") kg)"
    
        view.addInfoRow(key: "Weight", value: weightText)
        
        var abilitiesText = ""
        
        let abilities = pokemon.abilities ?? []
        
        for (index, ability) in abilities.enumerated() {
            if let abilityName = ability.ability?.name?.replacingOccurrences(of: "-", with: " ").capitalized {
                abilitiesText += index < abilities.count - 1 ? "\(abilityName), " : "\(abilityName)"
            }
        }
        
        view.addInfoRow(key: "Abilities", value: abilitiesText, numberOfLines: 3)
        
        if let info = pokemon.info {
            
            view.addTitle("Breeding")
            
            var eggText = ""
            
            let eggs = info.eggGroups ?? []
            
            for (index, egg) in eggs.enumerated() {
                if let eggName = egg.name?.capitalized {
                    eggText += index == eggs.count - 1 ? "\(eggName)" : "\(eggName), "
                }
            }
            
            if let habitat = info.habitat?.name?.replacingOccurrences(of: "-", with: " ").capitalized {
                view.addInfoRow(key: "Habitat", value: habitat)
            }
            
            view.addInfoRow(key: "Egg Groups", value: eggText, numberOfLines: 3)
            
            if let growth = info.growthRate?.name?.replacingOccurrences(of: "-", with: " ").capitalized {
                view.addInfoRow(key: "Growth Rate", value: growth)
            }
        }else{
            view.addNoConnectionView()
        }
    }
}

extension PokemonInfoViewModel: ReachabilityDelegate {
    func statusDidChanged(flag: SCNetworkReachabilityFlags) {
        delegate?.connectionDidChangeState(isConnected: flag.contains(.reachable))
    }
}
