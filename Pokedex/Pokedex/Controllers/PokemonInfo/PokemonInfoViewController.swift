//
//  PokemonInfoViewController.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import UIKit

class PokemonInfoViewController: UIViewController {
    
    var viewModel: PokemonInfoViewModel?
    
    var _view: PokemonInfoView? {
        return view as? PokemonInfoView
    }
    
    
    init(pokemon: Pokemon) {
        super.init(nibName: nil, bundle: nil)
        viewModel = PokemonInfoViewModel(pokemon: pokemon)
        viewModel?.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = PokemonInfoView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let view = _view else{
            return
        }
        viewModel?.configure(view: view)
    }
    
}

extension PokemonInfoViewController:  PokemonInfoViewModelDelegate {
    func connectionDidChangeState(isConnected: Bool) {
        if let view = _view, isConnected {
            viewModel?.reloadIfNeeded(view: view)
        }
    }
}
