//
//  PokemonInfoView.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Anchorage

class PokemonInfoView: UIView {
    
    private let stackView = UIStackView()

    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.contentInset = .init(top: 40, left: 0, bottom: 24, right: 0)
        
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 24
    
        scrollView.addSubview(stackView)
        stackView.horizontalAnchors /==/ scrollView.horizontalAnchors
        stackView.verticalAnchors /==/ scrollView.verticalAnchors
        stackView.centerXAnchor /==/ scrollView.centerXAnchor
        
        return scrollView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
        configureConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configureUI(){
        addSubview(scrollView)
    }
    
    func configureConstraints(){
        scrollView.verticalAnchors /==/ verticalAnchors
        scrollView.horizontalAnchors /==/ horizontalAnchors + 24
    }
    
    func addTitle(_ text: String){
        let view = UIView()
        
        let label = UILabel()
        label.text = text
        label.font = .boldSystemFont(ofSize: 25)
        label.textColor = .black
        
        view.addSubview(label)
        
        label.topAnchor /==/ view.topAnchor + 40
        label.horizontalAnchors /==/ view.horizontalAnchors
        label.bottomAnchor /==/ view.bottomAnchor
        
        stackView.addArrangedSubview(view)
    }
    
    func addInfoRow(key: String, value: String, numberOfLines lines: Int = 1) {
        let view = UIView()
        
        let labelDesc = UILabel()
        labelDesc.text = key
        labelDesc.textColor = UIColor.black.withAlphaComponent(0.5)
        labelDesc.font = .systemFont(ofSize: 15, weight: .semibold)
        
        let label = UILabel()
        label.text = value
        label.font = .boldSystemFont(ofSize: 16)
        label.textColor = .black
        label.numberOfLines = lines
        
        view.addSubview(labelDesc)
        view.addSubview(label)
        
        labelDesc.leftAnchor /==/ view.leftAnchor
        labelDesc.verticalAnchors /==/ view.verticalAnchors
        labelDesc.widthAnchor /==/ view.widthAnchor / 3.5
        
        label.leftAnchor /==/ labelDesc.rightAnchor + 16
        label.verticalAnchors /==/ view.verticalAnchors
        label.rightAnchor /==/ view.rightAnchor
        
        stackView.addArrangedSubview(view)
    }
    
    func addNoConnectionView(){
        let view = UIView()
        
        view.heightAnchor /==/ 75
        
        let labelDesc = UILabel()
        labelDesc.text = "No internet connection!"
        labelDesc.textColor = UIColor.black.withAlphaComponent(0.5)
        labelDesc.textAlignment = .center
        labelDesc.font = .systemFont(ofSize: 16, weight: .semibold)
        
        let imageView = UIImageView(image: UIImage(named: "psyduck"))
        
        view.addSubview(labelDesc)
        view.addSubview(imageView)
        
        imageView.leftAnchor /==/ view.leftAnchor
        imageView.verticalAnchors /==/ view.verticalAnchors
        imageView.heightAnchor /==/ imageView.widthAnchor
        
        labelDesc.leftAnchor /==/ imageView.rightAnchor
        labelDesc.verticalAnchors /==/ view.verticalAnchors
        labelDesc.rightAnchor /==/ view.rightAnchor
        
        stackView.addArrangedSubview(view)
    }
    
    func clearStackView(){
        for s in stackView.arrangedSubviews {
            stackView.removeArrangedSubview(s)
            s.removeFromSuperview()
        }

    }
}
