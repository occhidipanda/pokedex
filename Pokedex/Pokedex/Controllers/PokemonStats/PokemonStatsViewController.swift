//
//  PokemonStatsViewController.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import UIKit

class PokemonStatsViewController: UIViewController {
    
    var viewModel: PokemonStatsViewModel?
    
    var _view: PokemonStatsView? {
        return view as? PokemonStatsView
    }
    
    
    init(pokemon: Pokemon) {
        super.init(nibName: nil, bundle: nil)
        viewModel = PokemonStatsViewModel(with: pokemon)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = PokemonStatsView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let view = _view else{
            return
        }
        
        viewModel?.configure(view: view)
        
    }

}
