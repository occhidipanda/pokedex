//
//  PokemonStatsViewModel.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import UIKit

class PokemonStatsViewModel: NSObject {
    
    private var pokemon: Pokemon
    
    init(with pokemon: Pokemon) {
        self.pokemon = pokemon
        super.init()
    }
    
    func configure(view: PokemonStatsView){
        
        var total = [Int]()
        
        for stat in pokemon.stats ?? [] {
            if let name = stat.stat?.name, let st = Stats(rawValue: name), let value = stat.baseStat {
                view.addStatView(stat: st, value: value)
                total.append(value)
            }
        }
        
        let totalVal = total.reduce(0, +)
        view.addStatView(stat: .total, value: totalVal, color: pokemon.types?.first?.color, maxValue: CGFloat(255 * total.count))
        
    }
    
}
