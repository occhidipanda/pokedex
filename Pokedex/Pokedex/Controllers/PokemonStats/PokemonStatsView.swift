//
//  PokemonStatsView.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import Anchorage

class PokemonStatsView: UIView {
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 16
        return stackView
    }()
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.contentInset = .init(top: 32, left: 0, bottom: 24, right: 0)
        scrollView.addSubview(stackView)
        stackView.horizontalAnchors /==/ scrollView.horizontalAnchors
        stackView.verticalAnchors /==/ scrollView.verticalAnchors
        stackView.centerXAnchor /==/ scrollView.centerXAnchor
        return scrollView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
        configureConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configureUI(){
        addSubview(scrollView)
    }
    
    func configureConstraints(){
        scrollView.verticalAnchors /==/ verticalAnchors
        scrollView.horizontalAnchors /==/ horizontalAnchors + 24
    }
    
    func addStatView(stat: Stats, value: Int, color: UIColor? = nil, maxValue: CGFloat = 255){
        let view = UIView()
        
        view.heightAnchor /==/ 35
        
        let label = UILabel()
        label.text = stat.title.replacingOccurrences(of: "-", with: " ").capitalized
        label.textColor = UIColor.black.withAlphaComponent(0.5)
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        
        let labelValue = UILabel()
        labelValue.text = String(value)
        labelValue.textAlignment = .center
        labelValue.textColor = UIColor.black
        labelValue.font = .boldSystemFont(ofSize: 16)

        
        let progressView = ProgressBarView()
        progressView.backgroundColor = .clear
        progressView.currentValue = CGFloat(value)
        progressView.maxValue = maxValue
        
        if let color = color {
            progressView.barColor = color
        }else{
            progressView.barColor = stat.color ?? .green
        }
        
        view.addSubview(label)
        view.addSubview(labelValue)
        view.addSubview(progressView)
        
        label.leftAnchor /==/ view.leftAnchor
        label.verticalAnchors /==/ view.verticalAnchors
        label.widthAnchor /==/ 100
        
        labelValue.leftAnchor /==/ label.rightAnchor
        labelValue.widthAnchor /==/ 45
        labelValue.verticalAnchors /==/ view.verticalAnchors
        
        progressView.leftAnchor /==/ labelValue.rightAnchor
        progressView.heightAnchor /==/ 10
        progressView.centerYAnchor /==/ view.centerYAnchor
        progressView.rightAnchor /==/ view.rightAnchor
        
        stackView.addArrangedSubview(view)
    }
    
}
