//
//  ProgressBarView.swift
//  Pokedex
//
//  Created by Tiziano on 26/01/21.
//

import UIKit

class ProgressBarView: UIView {

    var maxValue: CGFloat = 255 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    var currentValue: CGFloat = 0 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    var barColor: UIColor = .green {
        didSet{
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let backgroundPath = UIBezierPath(roundedRect: rect, cornerRadius: rect.height / 2)
        UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1).setFill()
        backgroundPath.fill()
        
        let progressPath = UIBezierPath(roundedRect: CGRect(origin: rect.origin, size: CGSize(width: rect.width * CGFloat(currentValue / maxValue) , height: rect.height)), cornerRadius: rect.height / 2)
        barColor.setFill()
        progressPath.fill()
        
    }

}
