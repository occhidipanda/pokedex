//
//  NoConnectionView.swift
//  Pokedex
//
//  Created by Tiziano on 27/01/21.
//

import Anchorage

class NoConnectionView: UIView {

    let imageViewInfo = UIImageView(image: UIImage(named: "psyduck"))
    
    let labelTitle: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "No internet connection!\nGo outside, it's beautiful!"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = .lightGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI(){
        backgroundColor = .white
        addSubview(imageViewInfo)
        addSubview(labelTitle)
    }
    
    func setupConstraints(){
            
        imageViewInfo.centerAnchors /==/ centerAnchors
        
        labelTitle.topAnchor /==/ imageViewInfo.bottomAnchor + 8
        labelTitle.centerXAnchor /==/ centerXAnchor

    }

}
